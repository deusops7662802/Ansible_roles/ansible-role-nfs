Ansible role NFS
=========
Роль устанавливает NFS-сервер и NFS-клиент




Role Variables
--------------
Для роли требуется задать IP адрес NFS-сервера в
defaults/main.yml
nfs_server_ip:


Пример Playbook
----------------

Пример использования роли

    - hosts: servers
      roles:
         - nfs

License
-------
FOSS

==========
